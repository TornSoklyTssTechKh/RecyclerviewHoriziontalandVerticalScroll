package com.tsskh.recyclerviewsample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewHoriziontal;
    private RecyclerView mRecyclerViewVertical;

    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.LayoutManager mLayoutManagerVertical;
    private String[] myDataset ={"1","2","3","4","5"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerViewHoriziontal = (RecyclerView) findViewById(R.id.rcy_horiziontal);
        mRecyclerViewVertical = (RecyclerView) findViewById(R.id.rcy_vertical);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerViewHoriziontal.setHasFixedSize(true);
        mRecyclerViewVertical.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,true);
        mRecyclerViewHoriziontal.setLayoutManager(mLayoutManager);

        mLayoutManagerVertical = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true);
        mRecyclerViewVertical.setLayoutManager(mLayoutManagerVertical);

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(myDataset);
        mRecyclerViewHoriziontal.setAdapter(mAdapter);
        mRecyclerViewVertical.setAdapter(mAdapter);
    }
}
